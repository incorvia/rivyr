namespace :emails do
  desc "Send daily newsletters"
  task :send_newsletter => :environment do
    User.send_newsletter
  end
end
