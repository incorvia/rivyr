namespace :feeds do
  desc "Fetch and parse feeds."
  task :fetch => :environment do
    Feed.fetch!
  end
end
