module Rivyr
  module Search
    def self.settings
      {
        analysis: {
          analyzer: {
            feed_analyzer: {
              type: "custom",
              tokenizer: "classic",
              filter: ["standard", "lowercase", "word_delimiter", "unique"]
            },
            feed_keyword_analyzer: {
              type: "custom",
              tokenizer: "whitespace",
              filter: ["standard", "lowercase", "word_delimiter", "my_shingle"]
            }
          },
          filter: {
            my_shingle: {
              type: 'shingle',
              output_unigrams: false
            }
          }
        }
      }
    end
  end
end
