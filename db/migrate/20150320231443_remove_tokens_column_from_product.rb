class RemoveTokensColumnFromProduct < ActiveRecord::Migration
  def change
    remove_column :products, :tokens
  end
end
