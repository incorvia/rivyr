class AddEmailFrequencyToUser < ActiveRecord::Migration
  def change
    add_column :users, :email_frequency, :string, default: "daily", null: false
    add_index :users, :email_frequency
  end
end
