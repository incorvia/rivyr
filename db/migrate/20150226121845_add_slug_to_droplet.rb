class AddSlugToDroplet < ActiveRecord::Migration
  def change
    add_column :droplets, :slug, :string
    Droplet.all.map(&:save)
    change_column :droplets, :slug, :string, null: false
    add_index :droplets, :slug, unique: true
  end
end
