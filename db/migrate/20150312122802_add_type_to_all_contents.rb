class AddTypeToAllContents < ActiveRecord::Migration
  def change
    Content.update_all(type: 'Droplet')
  end
end
