class AddVanityIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :vanity_id, :integer
    add_index :products, :vanity_id
  end
end
