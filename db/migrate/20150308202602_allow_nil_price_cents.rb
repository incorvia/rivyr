class AllowNilPriceCents < ActiveRecord::Migration
  def change
    change_column :products, :price_cents, :integer, :null => true
  end
end
