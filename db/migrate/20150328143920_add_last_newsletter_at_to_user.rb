class AddLastNewsletterAtToUser < ActiveRecord::Migration
  def change
    add_column :users, :last_newsletter_at, :timestamp
    add_index :users, :last_newsletter_at
    User.update_all(last_newsletter_at: Time.now)
  end
end
