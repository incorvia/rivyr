class AddFollowersSupport < ActiveRecord::Migration
  def change
    create_join_table :products, :users do |t|
      t.index :product_id
      t.index :user_id
    end
    add_index :products_users, [ :product_id, :user_id ], :unique => true
  end
end
