class SlugChangesToDropletType < ActiveRecord::Migration
  def change
    add_column :product_categories, :icon, :string, null: false
    rename_column :droplet_types, :identifier, :slug

    remove_index :droplet_types, :slug
    add_index :droplet_types, :slug, unique: true
    remove_index :product_categories, :slug
    add_index :product_categories, :slug, unique: true
  end
end
