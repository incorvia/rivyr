class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :name, null: false
      t.string :description, null: false
      t.string :image_uid

      t.timestamps null: false
    end
  end
end
