class AddSourceUrlToDroplet < ActiveRecord::Migration
  def change
    add_column :droplets, :source_url, :string
  end
end
