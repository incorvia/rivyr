class ChangePriceCentsColumnToNotHaveDefault < ActiveRecord::Migration
  def change
    change_column_default :products, :price_cents, nil
    change_column :products, :price_cents, :integer, allow_nil: true
  end
end
