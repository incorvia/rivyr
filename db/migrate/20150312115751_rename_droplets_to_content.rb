class RenameDropletsToContent < ActiveRecord::Migration
  def change
    change_column :droplets, :slug, :string, null: true
    rename_table :droplets, :contents
    add_column :contents, :type, :string
    add_index :contents, :type
  end
end
