class AddSourceToDroplet < ActiveRecord::Migration
  def change
    add_column :droplets, :source, :string
    add_index :droplets, :source
  end
end
