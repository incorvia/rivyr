class AddCachedCommentsTotalToDroplet < ActiveRecord::Migration
  def change
    add_column :droplets, :cached_comments_total, :integer, default: 0
    add_index :droplets, :cached_comments_total
  end
end
