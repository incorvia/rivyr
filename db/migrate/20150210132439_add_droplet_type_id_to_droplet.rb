class AddDropletTypeIdToDroplet < ActiveRecord::Migration
  def change
    add_column :droplets, :droplet_type_id, :integer, null: false
    add_index :droplets, :droplet_type_id
  end
end
