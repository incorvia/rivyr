class CreateDropletTypes < ActiveRecord::Migration
  def change
    create_table :droplet_types do |t|
      t.string :name, null: false
      t.string :identifier, null: false

      t.timestamps null: false
    end
    add_index :droplet_types, :identifier, unique: true
  end
end
