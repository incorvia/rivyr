class AddIconToDropletType < ActiveRecord::Migration
  def change
    add_column :droplet_types, :icon, :string, default: "", null: false
  end
end
