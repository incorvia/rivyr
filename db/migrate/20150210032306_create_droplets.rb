class CreateDroplets < ActiveRecord::Migration
  def change
    create_table :droplets do |t|
      t.string :title, null: false
      t.text :content, null: false
      t.integer :product_id, null: false

      t.timestamps null: false
    end
    add_index :droplets, :product_id
  end
end
