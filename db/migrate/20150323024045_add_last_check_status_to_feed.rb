class AddLastCheckStatusToFeed < ActiveRecord::Migration
  def change
    add_column :feeds, :status, :string
    add_index :feeds, :status
  end
end
