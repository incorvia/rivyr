class AddCachedFollowerTotalToProduct < ActiveRecord::Migration
  def change
    add_column :products, :cached_follower_total, :integer, default: 0
    add_index :products, :cached_follower_total
  end
end
