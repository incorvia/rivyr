class AddTokensFieldToProduct < ActiveRecord::Migration
  def change
    add_column :products, :tokens, :string, array: true
    add_index :products, :tokens
  end
end
