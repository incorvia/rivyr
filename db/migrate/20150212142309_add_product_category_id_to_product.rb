class AddProductCategoryIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :product_category_id, :integer, null: false
    add_index :products, :product_category_id
  end
end
