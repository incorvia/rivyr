class AddDropletIdIndexToComments < ActiveRecord::Migration
  def change
    add_index :comments, :droplet_id
  end
end
