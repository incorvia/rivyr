# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

ActiveRecord::Base.transaction do
  # Users
  user = User.create(username: 'incorvia', password: 'password', password_confirmation: 'password', email: 'kevin@incorvia.me')
  admin = AdminUser.create(email: 'kevin@incorvia.me', password: 'password', password_confirmation: 'password')

  # PRODUCT_CATEGORIES
  pc1 = ProductCategory.create(name: 'Cameras', icon: "camera")
  pc2 = ProductCategory.create(name: 'Smartphones', icon: "phone-mobile")
  
  # DROPLET_TYPES
  dt1 = DropletType.create(name: "Review", icon: "graph-bar")
  dt2 = DropletType.create(name: "Price Change", icon: "dollar")
  dt3 = DropletType.create(name: "Rumor", icon: "comment")
  dt4 = DropletType.create(name: "Comparison", icon: "arrows-compress")
  dt5 = DropletType.create(name: "Announcement", icon: "rss")

  # PRODUCTS

  # Samsung NX500
  p1_vanity = Photo.create(name: "Samsung NX500 Black - Front Photo 1", description: "Stock photo of the Samsung NX500 in Black from Samsung Mobile Press.", image_url: "http://rivyr-public.s3.amazonaws.com/seeds/NX500_front_black_1.jpg")

  p1 = Product.create(name: "Samsung NX500", product_category: pc1, vanity_id: p1_vanity.id, asin: "B00SYKXMKS", price_cents: 79999)

  p1_droplet_1_title      = 'Samsung announces NX500 mirroless camera, spiritual successor to the wildly popular NX300'
  p1_droplet_1_content    = 'Samsung today announced its new mid-range mirrorless camera giving it most of the guts of their flagship NX1, but all the compactness and retro styling that casual consumers demand.'
  p1_droplet_1_source_url = 'http://www.samsungmobilepress.com/2015/02/05/Capture-Your-Signature-Moment-with-Samsung-NX500-1'
  p1.droplets.create(title: p1_droplet_1_title, content: p1_droplet_1_content, droplet_type_id: dt5.id, source_url: p1_droplet_1_source_url, created_at: Date.new(2015,2,5))

  p1_droplet_2_title      = 'Samsung makes NX500 available to purchase for $799.99'
  p1_droplet_2_content    = "Now you can be one of the first to own the NX500 by buying it directly at Samsung.com for $799.99. That's $200 more than the initial MSRP that the NX300 had which was to be expected as this camera is seen to have premium hardware over the NX300."
  p1_droplet_2_source_url = 'http://www.samsung.com/us/photography/digital-cameras/EV-NX500ZBMIUS'
  p1.droplets.create(title: p1_droplet_2_title, content: p1_droplet_2_content, droplet_type_id: dt2.id, source_url: p1_droplet_2_source_url, created_at: Date.new(2015,3,4))

  # Apple iPhone 6
  p2_vanity = Photo.create(name: "Apple iPhone 6 - Front Photo 1", description: "Stock photo of the Apple iPhone 6", image_url: "http://rivyr-public.s3.amazonaws.com/seeds/APPLE_IPHONE6_Homescreen_print.jpg")

  p2 = Product.create(name: "Apple iPhone 6", product_category: pc2, vanity_id: p2_vanity.id, asin: "B00NQGP42Y", price_cents: 71500)

  p2_droplet_1_title      = 'PhoneArena posts iPhone 6 vs Galaxy S6 Edge comparison'
  p2_droplet_1_source_url = 'http://www.phonearena.com/news/Samsung-Galaxy-S6-edge-vs-iPhone-6-a-real-life-speed-comparison_id66825'
  p2_droplet_1_content    = "PhoneArena wanted to know the effect Xiamoi's new UFS memory had on speed and what better to test it against than the iPhone 6? Check out Phone Arenan's video for a real life use case example."
  p2.droplets.create(title: p2_droplet_1_title, content: p2_droplet_1_content, droplet_type_id: dt4.id, source_url: p2_droplet_1_source_url, created_at: Date.new(2015,3,5))

  p2_droplet_2_title      = "Apple announces iPhone 6 at special event"
  p2_droplet_2_source_url = 'http://www.apple.com/pr/library/2014/10/13iPhone-6-iPhone-6-Plus-Arrive-in-36-More-Countries-and-Territories-This-Month.html'
  p2_droplet_2_content    = "Apple finally took the wraps off it's latest and greatest smartphone the iPhone 6 confirming much of what we've been hearing over the last few months, a bigger and bolder form factor sure to capture many consumers attention."
  p2.droplets.create(title: p2_droplet_2_title, content: p2_droplet_2_content, droplet_type_id: dt5.id, source_url: p2_droplet_2_source_url, created_at: Date.new(2014,9,9))

  p2_droplet_3_title      = "The Verge releases its review of iPhone 6: 9.0"
  p2_droplet_3_source_url = 'http://www.theverge.com/2014/9/16/6154975/iphone-6-review'
  p2_droplet_3_content    = "Highly anticipated, the first reviews of the iPhone 6 are surely to grab a lot of attention.  Today The Verge released a highly praiseful review of the new iPhone 6 giving it their highest marks for it's display and camera. While stating that there largely 'nothing truly ambitious', they easily come to the conclusion that this is 'one of the best smartphones on the market'"
  p2.droplets.create(title: p2_droplet_3_title, content: p2_droplet_3_content, droplet_type_id: dt1.id, source_url: p2_droplet_3_source_url, created_at: Date.new(2014,9,16))
end
