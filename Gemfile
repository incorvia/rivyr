source 'https://rubygems.org'

ruby '2.2.0'

# Rails
gem 'rails', '~> 4.2'
gem 'pg'
gem 'sass-rails'
gem 'uglifier'
gem 'coffee-rails'
gem 'jquery-rails'
gem 'jbuilder'
gem 'sdoc', group: :doc

# Cells
gem 'cells', github: 'incorvia/cells'
gem 'cells-slim', github: 'trailblazer/cells-slim'

# Active Admin
gem 'activeadmin', github: 'activeadmin'
gem 'devise', :git => 'https://github.com/plataformatec/devise.git'
gem 'devise-bootstrap-views'
gem 'formtastic', github: 'justinfrench/formtastic'
gem 'inherited_resources'

# Other
gem 'active_model_serializers'
gem 'acts-as-taggable-on'
gem 'algoliasearch-rails'
gem 'bootstrap-sass'
gem 'configatron'
gem 'dragonfly'
gem 'dragonfly-s3_data_store'
gem 'feedjira'
gem 'friendly_id'
gem 'gravatar_image_tag'
gem 'js-routes'
gem 'money-rails'
gem 'public_suffix'
gem 'responders'
gem 'searchkick'
gem 'slim-rails'
gem 'kaminari'
gem 'mandrill-api', require: 'mandrill'

# Comments
gem 'closure_tree'
gem 'acts_as_votable'

# Metrics
gem 'newrelic_rpm'

group :production do
  gem 'rollbar'
  gem 'rails_12factor'
  gem 'puma'
end

group :development do
  gem 'rails-footnotes'
  gem 'guard'
  gem 'guard-minitest'
  gem "letter_opener"
end

group :development, :test do
  gem 'database_cleaner'
  gem 'dotenv-rails'
  gem 'factory_girl_rails'
  gem 'minitest-focus'
  gem 'minitest-rails-capybara'
  gem 'minitest-spec-rails'
  gem 'mocha'
  gem 'poltergeist'
  gem 'pry-nav'
  gem 'pry-rails'
  gem 'spring'
  gem 'terminal-notifier'
  gem 'terminal-notifier-guard'
  gem 'web-console'
end

