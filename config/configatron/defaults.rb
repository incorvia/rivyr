# Put all your default configatron settings here.

# Example:
#   configatron.emails.welcome.subject = 'Welcome!'
#   configatron.emails.sales_reciept.subject = 'Thanks for your order'
#
#   configatron.file.storage = :s3

configatron.app.name = "rivyr"

configatron.env.algoliasearch_application_id = ENV['ALGOLIASEARCH_APPLICATION_ID']
configatron.env.algoliasearch_api_key = ENV['ALGOLIASEARCH_API_KEY']
configatron.env.algoliasearch_api_key_search = ENV['ALGOLIASEARCH_API_KEY_SEARCH']

configatron.env.amazon_key = ENV['AMAZON_KEY']
configatron.env.amazon_secret = ENV['AMAZON_SECRET']

configatron.amazon.associate_id = 'rivyr01-20'

configatron.dragonfly.root = 'public/images/df'

configatron.social.facebook = 'rivyr'
configatron.social.twitter = 'rivyr_app'
configatron.social.google_plus = '+Rivyr-App'

configatron.social.facebook_url = "https://www.facebook.com/#{configatron.social.facebook}"
configatron.social.twitter_url = "https://www.twitter.com/#{configatron.social.twitter}"
configatron.social.google_plus_url = "https://plus.google.com/#{configatron.social.google_plus}"

configatron.feeds.hit_threshold = (ENV['FEED_THRESHOLD'] || 0.1).to_f

configatron.default_host = 'https://www.rivyr.com'
