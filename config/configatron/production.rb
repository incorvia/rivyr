# Override your default settings for the Production environment here.
#
# Example:
#   configatron.file.storage = :s3

configatron.s3.bucket = 'rivyr-production'
configatron.dragonfly.root = 'images/df'

ActionMailer::Base.smtp_settings = {
    :port =>           '587',
    :address =>        'smtp.mandrillapp.com',
    :user_name =>      ENV['MANDRILL_USERNAME'],
    :password =>       ENV['MANDRILL_APIKEY'],
    :domain =>         'localhost',
    :authentication => :plain
}
ActionMailer::Base.delivery_method = :smtp
