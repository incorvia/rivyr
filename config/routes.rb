Rails.application.routes.draw do
  # Devise
  devise_for :users, controllers: { 
    registrations: "users/registrations",
    sessions: "users/sessions"
  }
  devise_for :admin_users, ActiveAdmin::Devise.config

  # Active Admin
  ActiveAdmin.routes(self)

  # Root
  authenticated :user do
    root to: "users#show", as: :authenticated_root
  end

  unauthenticated :user do
    root to: "pages#home", as: :user
    root to: "pages#home"
  end

  # Products
  resources :products, only: [:show] do
    resources :droplets, only: [:show], as: :droplet
  end
  post 'products/:id/follow', to: 'products#follow', as: :follow_product, constraints: { format: 'json' }
  post 'products/:id/unfollow', to: 'products#unfollow', as: :unfollow_product, constraints: { format: 'json' }

  # Droplets
  resources :droplets, only: [] do
    resources :comments, only: [:create, :update, :destroy]
  end

  # Search
  get '/search', to: "search#index", as: :search

  # Dragonfly
  get '/products/:id/thumb/:style.:format' => Dragonfly.app.endpoint { |params, app|
    Product.find(params[:id]).vanity_safe.image.thumb(params[:style]).encode(params[:format])
  }, as: :product_thumb
end
