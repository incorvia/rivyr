require 'dragonfly'

# Configure
Dragonfly.app.configure do
  plugin :imagemagick

  secret "b52915ba4e99b6bff6095fef4d9424c98d3d206510f1f3b90347b23e92b55269"

  url_format "/media/:job/:name"

  fetch_file_whitelist [/assets/]

  case Rails.env
  when 'test'
    datastore :memory
  when 'production'
    datastore :s3,
      bucket_name: configatron.s3.bucket,
      access_key_id: configatron.env.amazon_key,
      secret_access_key: configatron.env.amazon_secret,
      root_path: configatron.dragonfly.root
  else
    datastore :file,
      root_path: configatron.dragonfly.root,
      server_root: Rails.root.join('public')
  end
end

# Logger
Dragonfly.logger = Rails.logger

# Mount as middleware
Rails.application.middleware.use Dragonfly::Middleware

# Add model functionality
if defined?(ActiveRecord::Base)
  ActiveRecord::Base.extend Dragonfly::Model
  ActiveRecord::Base.extend Dragonfly::Model::Validations
end
