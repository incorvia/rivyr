AlgoliaSearch.configuration = {
  application_id: configatron.env.algoliasearch_application_id,
  api_key: configatron.env.algoliasearch_api_key
}
