require 'test_helper'

class NavbarCellTest < ActiveSupport::TestCase
  describe 'show' do
    let(:rendered) {
      out = cell(:navbar).call
      Capybara.string(out)
    }

    it 'renders the brand name' do
      rendered.find('.navbar-brand').text.must_equal('rivyr')
    end

    it 'renders a category list' do
      create(:product_category, name: 'Category')
      rendered.find('.nav-category-links').text.strip.must_equal("Category")
    end

    describe 'logged out' do
      it 'renders a signup link' do
        rendered.find('#nav-signup').text.strip.must_equal('Sign Up')
      end

      it 'renders a my rivyr link' do
        rendered.find('#nav-signin').text.strip.must_equal('My Rivyr')
      end
    end

    describe 'logged in' do
      let(:rendered) {
        out = cell(:navbar).show(current_user: build(:user))
        Capybara.string(out)
      }

      it 'renders Account options' do
        rendered.find('#nav-account').text.strip.must_equal('Account')
      end

      it 'renders Log out options' do
        rendered.find('#nav-logout').text.strip.must_equal('Log out')
      end
    end
  end
end
