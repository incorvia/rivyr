require 'test_helper'

class StreamCellTest < ActiveSupport::TestCase
  describe 'show' do
    let(:droplet) { create(:droplet) }
    let(:rendered) {
      out = cell(:stream, Kaminari.paginate_array([droplet]).page(1)).call
      Capybara.string(out)
    }

    it 'renders the stream' do
      rendered.find('.tm-stream').find('.droplet-title').text.must_equal(droplet.title)
    end
  end
end
