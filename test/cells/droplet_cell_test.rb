require 'test_helper'

class DropletCellTest < ActiveSupport::TestCase
  describe 'show' do
    let(:droplet) { create(:droplet) }

    let(:rendered) {
      out = cell(:droplet, droplet).call
      Capybara.string(out)
    }

    it 'renders the title' do
      rendered.find('.droplet-title').text.must_equal(droplet.title)
    end

    it 'renders the content' do
      rendered.find('.droplet-content').text.must_equal(droplet.content)
    end

    describe 'head' do
      it 'renders the correct icon' do
        rendered.find('.icon-review').wont_be_nil
      end
    end

    describe 'brand_header' do
      let(:rendered) {
        out = cell(:droplet, droplet).show(brand_header: true)
        Capybara.string(out)
      }

      it 'renders the brand header' do
        out = rendered.find('.droplet-brand-header')
        out.find('img').wont_be_nil
        out.text.squish.match(/Product \d* - Name \d*/).wont_be_nil
      end
    end
  end
end
