require 'test_helper'

class FacetSetCellTest < ActiveSupport::TestCase
  describe 'show' do
    let(:product) { create(:product_with_droplets) }

    let(:rendered) {
      out = cell(:facet_set, product).show(filters: product.droplet_types_with_counts, params_key: :category, controller: 'search')
      Capybara.string(out)
    }

    it "renders filter icons" do
      rendered.find_css(".icon-review").length.must_equal(1)
    end

    it "renders the correct filter text" do
      DropletType.any_instance.stubs(:name).returns("Reviews")
      rendered.find_css(".filter-text").text.strip.must_equal("Reviews (1)")
    end
  end
end
