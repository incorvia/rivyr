require "test_helper"

class ProductTest < ActiveSupport::TestCase
  let(:product) { create(:product) }

  describe '.query_text' do
    it 'must return filter lowercase words' do
      out = Product.feed_sanitize("This is the Nexus 5S by Samsung, release date Nov 12")
      out.must_equal("nexus 5s samsung, date 12")
    end
  end

  describe '#product_types_with_counts' do
    let(:product) { create(:product_with_droplets) }

    it 'returns the count of droplet types' do
      types = product.droplet_types_with_counts
      types.length.must_equal(1)
      types.first.count.must_equal(1)
    end
  end

  describe '#amazon_url' do
    it 'returns an amazon formatted url' do
      product.amazon_url.must_equal("http://www.amazon.com/dp/#{product.asin}/?tag=#{configatron.amazon.associate_id}")
    end
  
    it 'returns nil when new asin is set' do
      product.asin = nil
      product.amazon_url.must_equal(nil)
    end
  end

  describe '#vanity_safe 'do
    it 'returns a new vanity if no vanity is present' do
      product.vanity_id = nil
      vanity = product.vanity_safe
      vanity.new_record?.must_equal(true)
    end

    it 'returns vanity if present' do
      product.stubs(:vanity).returns('safe')
      product.vanity_safe.must_equal('safe')
    end
  end

  describe '#should_generate_new_friendly_id?' do
    it 'should generate a new slug when slug is blank' do
      product.slug = ''
      product.valid?
      product.slug.blank?.wont_equal(true)
    end
  end
end
