require "test_helper"

describe Entry do
  let(:feed) { Feed.new }

  describe '.type_id' do
    it 'returns the correct type' do
      Feed.stubs(:droplet_types).returns({ 'review' => 1, 'rumor' => 2, 'general' => 3})
      Entry.new(OpenStruct.new(title: "Review")).type_id.must_equal(1)
      Entry.new(OpenStruct.new(title: "Rumor")).type_id.must_equal(2)
      Entry.new(OpenStruct.new(title: "Else")).type_id.must_equal(3)
    end
  end
end
