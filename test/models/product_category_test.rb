require "test_helper"

class ProductCategoryTest < ActiveSupport::TestCase
  let(:product_category) { build(:product_category) }
  describe 'should_generate_new_friendly_id?' do
    it 'should generate a new slug when slug is blank' do
      product_category.slug = ''
      product_category.valid?
      product_category.slug.blank?.wont_equal(true)
    end
  end
end
