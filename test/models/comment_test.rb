require "test_helper"

describe Comment do
  let(:droplet) { create(:droplet) }

  describe 'update_droplet_comment_count' do
    it 'updates the cached comments count on droplet on create and destroy' do
      comment = droplet.comments.create(user_id: 1, body: 'test')
      droplet.reload
      droplet.cached_comments_total.must_equal(1)
      comment.destroy
      droplet.cached_comments_total.must_equal(0)
    end
  end
end
