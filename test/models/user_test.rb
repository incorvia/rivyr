require "test_helper"

class UserTest < ActiveSupport::TestCase
  let(:user) { build(:user) }
  let(:product) { build(:product) }

  it 'must be valid' do
    user.valid?.must_equal(true)
  end

  describe '#username' do
    it 'must be longer than 3 characters' do
      user.username = (0...2).map {'a'}.join
      user.valid?.must_equal(false)
    end

    it 'must be shorter than 18 characters' do
      user.username = (0...19).map {'a'}.join
      user.valid?.must_equal(false)
    end

    it 'must be automatically downcased' do
      user.username = 'Abcd'
      user.valid?
      user.username.must_equal('abcd')
    end
  end

  describe '#following?' do
    let(:product) { create(:product) }
    let(:user) { create(:user) }

    it 'returns true of a user is following a product' do
      user.products << product
      user.following?(product).must_equal(true)
    end

    it 'returns false of a user is not following a product' do
      user.following?(product).must_equal(false)
    end
  end

  describe 'update_follower_count!' do
    it 'updates the follow count on the product' do
      user.save && product.save
      user.follow(product)
      product.cached_follower_total.must_equal(1)
    end
  end

  describe 'last_update_time' do
    let(:current_time) { Time.now }

    it 'returns the last_sign_in_at when it is greater' do
      user.last_sign_in_at = current_time
      user.last_newsletter_at = current_time - 10.seconds
      user.last_update_time.must_equal(current_time)
    end

    it 'returns the last_newsletter_time when it is greater' do
      user.last_newsletter_at = current_time
      user.last_sign_in_at = current_time - 10.seconds
      user.last_update_time.must_equal(current_time)
    end
  end
end
