require "test_helper"

describe Droplet do
  let(:droplet) { build(:droplet) }

  describe 'should_generate_new_friendly_id?' do
    it 'should generate a new slug when slug is blank' do
      droplet.slug = ''
      droplet.valid?
      droplet.slug.blank?.wont_equal(true)
    end
  end
end
