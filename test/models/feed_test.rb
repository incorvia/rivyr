require "test_helper"

describe Feed do
  let(:feed) { Feed.new }

  describe '.droplet_types' do
    it 'must return hash of droplet_types' do
      type = create(:droplet_type, name: 'Test') 
      Feed.droplet_types['test'].must_equal(type.id)
    end
  end
end
