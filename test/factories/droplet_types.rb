FactoryGirl.define do
  factory :droplet_type do
    sequence :name do |n|
      "Name #{n}"
    end
    icon "review"
  end
end
