FactoryGirl.define do
  factory :product do
    sequence :name do |n|
      "Product #{n}"
    end

    asin "B00SYKXMKS"
    vanity_id { create(:photo).id }
    product_category { create(:product_category) }

    factory :product_with_droplets do
      transient do
        droplets_count 1
      end

      after(:create) do |product, evaluator|
        create_list(:droplet, evaluator.droplets_count, product: product)
      end
    end
  end
end

