FactoryGirl.define do
  factory :photo do
    name "Test Photo"
    description "Description for test photo."
  end
end
