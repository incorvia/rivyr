FactoryGirl.define do
  factory :user do
    sequence :username do |n|
      "username_#{n}"
    end
    sequence :email do |n|
      "email_#{n}@example.com"
    end
    password "password123"
    password_confirmation "password123"
  end
end
