FactoryGirl.define do
  factory :droplet do
    sequence :title do |n|
      "Droplet Name #{n}"
    end
    content "Droplet content goes here."
    droplet_type { build(:droplet_type) }
    product
  end
end
