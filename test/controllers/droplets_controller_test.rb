require "test_helper"

describe DropletsController do
  describe '#show' do
    let(:product) { create(:product_with_droplets) }

    it "should get show" do
      get :show, id: product.droplets.first, product_id: product
      assert_response :success
    end
  end
end
