require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  let(:product) { create(:product) }
  let(:user) { create(:user) }

  describe '#show' do
    let(:product) { create(:product_with_droplets) }

    it "should get show" do
      get :show, id: product
      assert_response :success
    end
  end

  describe '#follow' do
    before { user.products.wont_include(product) }

    describe 'logged in' do
      before { sign_in user}

      it 'follows a product' do
        post :follow, id: product, format: :json
        user.products.must_include(product)
        JSON.parse(response.body)["following"].must_equal(true)
      end
    end

    describe 'logged out' do
      it 'redirects to login' do
        post :follow, id: product
        user.products.wont_include(product)
        response.status.must_equal(302)
      end
    end
  end

  describe '#unfollow' do
    before { user.follow(product) }

    describe 'logged in' do
      before { sign_in user}

      it 'unfollows a product' do
        post :unfollow, id: product, format: :json
        user.products.wont_include(product)
        JSON.parse(response.body)["following"].must_equal(false)
      end
    end

    describe 'logged out' do
      it 'renders a 401 unauthorized' do
        post :unfollow, id: product
        user.products.must_include(product)
        response.status.must_equal(302)
      end
    end
  end
end
