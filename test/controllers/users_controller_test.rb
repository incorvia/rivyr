require "test_helper"

class UsersControllerTest < ActionController::TestCase
  let(:user) { create(:user) }

  before { sign_in(user) }

  it "should get show" do
    get :show
    assert_response :success
  end
end
