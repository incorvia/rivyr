require 'test_helper'

class FollowLinkTest < Capybara::Rails::TestCase
  let(:product) { create(:product) }
  let(:user) { create(:user) }
  
  before { sign_in(user) }

  it 'follows a product', js: true do
    user.following?(product).must_equal(false)
    visit product_path(product)
    click_link('Follow')
    page.find('.following')
    user.following?(product).must_equal(true)
  end

  it 'unfollows a product', js: true do
    user.follow(product)
    visit product_path(product)
    click_link('Follow')
    page.find('.not-following')
    user.following?(product).must_equal(false)
  end
end

