ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "mocha/mini_test"
require "minitest/pride"
require "minitest/rails/capybara"
require 'database_cleaner'
require 'capybara/poltergeist'
require 'cell/test_case'

Capybara.javascript_driver = :poltergeist

class ActiveSupport::TestCase
  include Cell::Testing
  include FactoryGirl::Syntax::Methods
  include MiniTest::Metadata

  ActiveRecord::Migration.check_pending!

  self.use_transactional_fixtures = false

  before { DatabaseCleaner.start }
  after  { DatabaseCleaner.clean }

  def self.controller_class
    TestController
  end
end

class ActionController::TestCase
  include Devise::TestHelpers
end

class Capybara::Rails::TestCase
  before {
    Capybara.current_driver = :poltergeist
    DatabaseCleaner.clean # end current transaction
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.start
  }

  def sign_in(user)
    visit new_user_session_path
    fill_in('user[username]', with: user.username)
    fill_in('user[password]', with: "password123")
    click_button('Sign in')
  end
end

class TestController < ApplicationController
  def self.url_options
    {:host=>"localhost", :port=>3000, :protocol=>"http://", :script_name=>""}
  end
end
