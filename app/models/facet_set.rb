class FacetSet
  attr_accessor :set

  delegate *Array.instance_methods(false), to: :set

  def initialize(facets, klass)
    @set = []
    if facets
      objs = klass.where(slug: facets.keys)
      @set = facets.to_a.map do |facet, count|
        obj = objs.select { |o| o.slug == facet }.first
        struct = {}
        struct[:name] = obj.name
        struct[:icon] = obj.icon
        struct[:count] = count
        struct[:slug] = obj.slug
        OpenStruct.new(struct)
      end
    end
  end
end

