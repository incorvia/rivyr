class Photo < ActiveRecord::Base
  acts_as_taggable

  dragonfly_accessor :image do
    default "app/assets/images/default_vanity.jpg"
  end

  validates_presence_of :name, :description
end
