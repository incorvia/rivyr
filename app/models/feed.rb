class Feed < ActiveRecord::Base
  def self.fetch!
    Feed.all.map(&:fetch!)
  end

  def self.droplet_types
    @droplet_types ||= DropletType.all.each_with_object({}) do |type, hash|
      hash[type.slug] = type.id
    end
  end

  def feed
    @feed ||= Feedjira::Feed.fetch_and_parse([self.url]).values.first
  end

  def fetch!
    self.status = 'success'

    feed.entries.each do |entry|
      _entry = Entry.new(entry)

      begin
        _entry.create_suggestion! if _entry.create_suggestion?(last_checked_with_default)
      rescue => e
        Rails.logger.error "Error fetched feed #{self.name}: #{e.message}"
        self.status = 'error'
      end
    end

    self.last_checked = Time.now
    self.save
  end

  def last_checked_with_default
    last_checked || Time.now - 1.day
  end
end
