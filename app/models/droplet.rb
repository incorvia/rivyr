class Droplet < Content
  extend FriendlyId

  acts_as_votable

  has_many :comments

  friendly_id :slug_candidates, :use => [:slugged, :finders]

  def comment_hash
    comments.includes(:user).hash_tree
  end

  def update_cached_comments_total!
    self.update_attribute(:cached_comments_total, comments.count)
  end

  private

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end
end
