class User < ActiveRecord::Base
  include Streamable
  include Newsletterable

  EMAIL_FREQUENCIES = %w(none daily weekly)

  acts_as_voter

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable,
         :registerable,
         :rememberable,
         :trackable,
         :validatable,
         :authentication_keys => [:username]

  has_and_belongs_to_many :products, after_add: :update_follower_count!,
                                     after_remove: :update_follower_count!
  has_many :comments
  has_many :droplets, through: :products

  validates :username, {
    length: { minimum: 3, maximum: 18 },
    uniqueness: true,
    presence: true,
    format: { 
      with: /\A[a-z0-9_]*\z/,
      message: "A username can only contain alphanumeric characters (letters A-Z, numbers 0-9) with the exception of underscores."
    }
  }

  validates :email_frequency, { 
    inclusion: { in: EMAIL_FREQUENCIES, message: "%{value} is not a valid size" }
  }

  before_save { self.username.try(:downcase!) }
  before_save { self.last_newsletter_at = Time.now if !self.last_newsletter_at }

  def self.daily_newsletter
    where(email_frequency: 'daily')
  end

  def self.weekly_newsletter
    where(email_frequency: 'weekly')
  end

  def latest_updates(count)
    self.stream.where("contents.created_at > ?", last_update_time).limit(count)
  end

  def last_update_time
    last_newsletter_at > last_sign_in_at ? last_newsletter_at : last_sign_in_at
  end

  def following?(product)
    self.products.where("products.id = ?", product.try(:id)).present?
  end

  def follow(product)
    self.products << product unless following?(product)
  end

  def unfollow(product)
    self.products.destroy(product) if following?(product)
  end

  private

  def update_follower_count!(product)
    product.update_follower_count!
  end
end
