class Content < ActiveRecord::Base
  extend FriendlyId

  friendly_id :slug_candidates, :use => [:slugged, :finders]

  belongs_to :droplet_type
  belongs_to :product
  belongs_to :user

  validates_presence_of :title, :content, :product_id

  delegate :icon, to: :droplet_type, allow_nil: true
  delegate :name, :slug, to: :droplet_type, allow_nil: true, prefix: :type

  before_save :set_source

  def self.droplets
    where(type: 'Droplet')
  end

  def self.suggestions
    where(type: 'Suggestion')
  end

  def self.content_types
    self.subclasses.map(&:name)
  end

  def self.of_types(types=[])
    q = includes(:droplet_type).references(:droplet_type)
    q = q.where("droplet_types.slug" => types) if types.length > 0
    return q
  end

  private

  def slug_candidates
    [
      [:title, :secure_hex]
    ]
  end

  def secure_hex
    SecureRandom.hex(5)
  end

  def set_source
    if source_url
      self.source = PublicSuffix.parse(URI(source_url).hostname).sld rescue nil
    end
  end
end
