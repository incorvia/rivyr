class Comment < ActiveRecord::Base
  has_closure_tree order: 'created_at desc'

  belongs_to :droplet
  belongs_to :user

  validates_presence_of :user_id, :droplet_id

  after_create :update_droplet_comments_total!
  after_destroy :update_droplet_comments_total!

  private

  def update_droplet_comments_total!
    droplet.update_cached_comments_total!
  end
end
