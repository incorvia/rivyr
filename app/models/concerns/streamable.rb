# Streamable models must have_many :droplets

module Streamable
  extend ActiveSupport::Concern

  included do
    has_many :droplet_types, -> { uniq }, through: :droplets
  end

  def stream(types=[])
    droplets.
    preload(product: :vanity).
    of_types([types].compact.flatten).
    order(created_at: :desc)
  end

  def droplet_types_with_counts
    @counts ||= begin
      results = droplet_types.with_counts.order(name: :asc)
      set = []
      results.map do |obj|
        struct = {}
        struct[:name] = obj.name
        struct[:icon] = obj.icon
        struct[:count] = obj.type_count
        struct[:slug] = obj.slug
        OpenStruct.new(struct)
      end
    end
  end
end
