module ProductSearchable
  extend ActiveSupport::Concern

  included do
    searchkick index_name: "rivyr_products",
      mappings: Search::ProductSerializer.mapping,
      settings: Rivyr::Search.settings
  end

  class_methods do
    def feed_search(query)
      Product.search({
        body: {
          query: {
            filtered: {
              query: {
                match: {
                  "name.feed" => query
                }
              },
              filter: {
                terms: {
                  "name.feed_keywords" => Product.searchkick_index.tokens(query, analyzer: "feed_keyword_analyzer")
                }
              }
            }
          }
        }
      })
    end

    def feed_sanitized_search(query)
      Product.feed_search(self.feed_sanitize(query))
    end

    def feed_sanitize(query)
      keywords = query.split(' ').map(&:downcase)

      keywords = keywords.delete_if do |x|
        self.stop_words.include?(x)
      end

      keywords.join(' ')
    end

    def stop_words
      %w(
        be by can of is on to than the this
        january february march april may june july august september november december
        jan feb mar aug sept nov dec
        release released
        .,-
      )
    end
  end

  def search_data
    Search::ProductSerializer.new(self, root: false).as_json
  end
end
