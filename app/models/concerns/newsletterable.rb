# Streamable models must have_many :droplets

module Newsletterable
  extend ActiveSupport::Concern
  
  class_methods do
    def send_newsletter
      send_daily_newsletter
      send_weekly_newsletter
    end

    def send_daily_newsletter
      daily = self.daily_newsletter
      daily.each do |user|
        UpdateEmail.new(user).send_email
      end
    end

    def send_weekly_newsletter
      if Time.now.wday == 0
        weekly = self.weekly_newsletter
        weekly.each do |user|
          UpdateEmail.new(user).send_email
        end
      end
    end
  end
end
