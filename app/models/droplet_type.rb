class DropletType < ActiveRecord::Base
  extend FriendlyId

  has_many :droplets

  validates_presence_of :name, :icon
  validates_uniqueness_of :name

  friendly_id :name, :use => [:slugged, :finders]

  # Must be use in a join with droplets
  def self.with_counts
    select("droplet_types.*").
    select("count(contents.id) OVER (PARTITION BY droplet_types.slug) AS type_count")
  end
end
