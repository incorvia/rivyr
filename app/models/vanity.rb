class Vanity < Photo
  dragonfly_accessor :image do
    default "app/assets/images/default_vanity.jpg"
  end
end
