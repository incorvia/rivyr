class ProductCategory < ActiveRecord::Base
  extend FriendlyId

  validates_presence_of :name, :slug

  has_many :products

  friendly_id :name, :use => [:slugged, :finders]

  private

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end
end
