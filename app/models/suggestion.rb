class Suggestion < Content
  validates_presence_of :source_url

  friendly_id :slug_candidates, :use => [:slugged, :finders]

  private

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end
end
