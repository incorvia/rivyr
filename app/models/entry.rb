class Entry
  attr_accessor :entry
  attr_reader :product, :hit

  delegate :published, :title, :url, to: :entry

  def initialize(entry)
    @entry = entry
  end

  def create_suggestion?(date)
    (self.published > date) && product
  end

  def create_suggestion!
    Suggestion.new.tap do |suggestion|
      suggestion.title = title
      suggestion.content = content
      suggestion.source_url = url
      suggestion.droplet_type_id = type_id
      suggestion.product = product
    end.save
  end

  def content
    text = @entry.try(:content) || @entry.try(:summary) || @entry.try(:description)
    ActionView::Base.full_sanitizer.sanitize(text[0..800]).squeeze(' ').strip rescue nil
  end

  def product
    @product ||= begin
      product,hit = Product.feed_sanitized_search(self.title).each_with_hit.first
      product if product && hit['_score'] > configatron.feeds.hit_threshold
    end
  end

  def type_id
    case 
    when title =~ /review/i
      Feed.droplet_types['review']
    when title =~ /rumor/i
      Feed.droplet_types['rumor']
    when title =~ /\svs\.?\s/i
      Feed.droplet_types['comparison']
    else
      Feed.droplet_types['general']
    end
  end
end
