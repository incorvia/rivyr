class Product < ActiveRecord::Base
  extend FriendlyId
  include Streamable
  include AlgoliaSearch
  include ProductSearchable

  belongs_to :product_category
  belongs_to :vanity, class_name: "Photo"
  has_many :droplets
  has_many :suggestions

  has_and_belongs_to_many :users

  validates_presence_of :name, :product_category_id, :slug

  monetize :price_cents, allow_nil: true

  friendly_id :name, :use => [:slugged, :finders]

  delegate :slug, to: :product_category, allow_nil: true, prefix: :category

  algoliasearch per_environment: true, disable_indexing: Rails.env.test? do
    hitsPerPage 30

    tags do
      [category_slug]
    end

    attributesForFaceting [:category]

    attribute :name
    attribute :url do
      Rails.application.routes.url_helpers.product_path(slug)
    end
    attribute :category do
      category_slug
    end
  end

  scope :vanityless, -> { where(vanity_id: nil) }
  scope :asinless, -> { where(asin: [nil,""]) }

  def self.rivyr_raw(*opts)
    response = self.raw_search(*opts)
    hit_ids = response['hits'].map { |p| p['objectID'].to_i }
    response['hit_ids'] = hit_ids
    return response
  end

  def vanity_id_safe
    vanity_id || configatron.photos.default_vanity_photo_id
  end

  def vanity_safe
    vanity || Vanity.new
  end

  def amazon_url
    if asin.present?
      "http://www.amazon.com/dp/#{asin}/?tag=#{configatron.amazon.associate_id}"
    end
  end

  def update_follower_count!
    self.update_attribute(:cached_follower_total, self.users.count)
  end

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end
end
