class BuyLinkCell < Cell::ViewModel
  include MoneyRails::ActionViewExtension

  def show(url: nil)
    @url = url
    render
  end
end
