class FacetSetCell < ApplicationCell

  def show(params_key: nil, filters: [], path: "/", q: nil, controller: nil, action: nil)
    @q = q
    @params_key = params_key
    @path = path
    @filters = filters
    @active = [params[@params_key]].compact.flatten
    @page = params[:page]
    @controller = controller
    @action = action
    render
  end

  private

  def filter_off?(slug)
    !filter_on?(slug)
  end

  def filter_on?(slug)
    @active.include?(slug)
  end

  def filter_link(slug)
    params = {}
    params[:controller] = @controller if @controller
    params[:action] = @action if @action
    params[:q] = @q if @q.present?
    params[@params_key] = filter_on?(slug) ? @active - [slug] : @active + [slug]
    search_url_for(params)
  end
end
