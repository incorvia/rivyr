class SidebarCell < ApplicationCell

  def show(current_user: nil, title: nil)
    @title = title
    @current_user = current_user
    render
  end
end
