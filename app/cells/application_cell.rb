class ApplicationCell < Cell::ViewModel
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::TextHelper
  include Sprockets::Rails::Helper

  self.assets_environment = Rails.application.assets
  self.assets_prefix = Rails.application.config.assets.prefix
  self.digest_assets = Rails.application.config.assets[:digest]

  private

  def search_url_for(options={})
    url = url_for(options)
    url.last == '?' ? url[0..-2] : url
  end

  def categories
    ProductCategory.all
  end

  def current_user
    @current_user
  end

  def current_username
    current_user.try(:username)
  end

  def user_signed_in?
    current_user
  end
end
