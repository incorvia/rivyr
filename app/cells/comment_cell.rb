class CommentCell < ApplicationCell
  include ::GravatarImageTag

  def show
    render
  end

  private

  property :user
  property :created_at
  property :body
  property :cached_votes_total

  def time_ago
    time_ago_in_words(created_at)
  end
end
