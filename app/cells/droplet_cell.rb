class DropletCell < ApplicationCell

  def show(last: nil, brand_header: false)
    @brand_header = brand_header
    @last = last
    render
  end

  def mast
    render
  end

  private

  property :cached_comments_total
  property :content
  property :created_at
  property :icon
  property :source
  property :source_url
  property :title
  property :type_name
  property :type_slug

  def title_url
    source_url || product_droplet_path(product, model)
  end

  def product
    model.product
  end

  def vanity
    product.try(:vanity_safe)
  end

  def date
    created_at.strftime("%-m/%-d/%-y")
  end

  def last
    @last
  end
end
