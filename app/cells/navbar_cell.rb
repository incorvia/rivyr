class NavbarCell < ApplicationCell
  def show(current_user: nil)
    @current_user = current_user
    render
  end
end
