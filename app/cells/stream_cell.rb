class StreamCell < ApplicationCell

  def show(brand_headers: false)
    @brand_headers = brand_headers
    render
  end

  private

  property :num_pages
  property :current_page
  property :next_page
  property :prev_page
  property :first_page?
  property :last_page?

  def next_url
    url_for(params.merge(page: next_page))
  end

  def prev_url
    url_params = params.merge(page: prev_page)
    url_params.delete(:page) if prev_page == 1
    url_for(url_params)
  end

  def droplets
    model.each_with_index.map do |droplet, index|
      cell(:droplet, droplet).show(last: last?(index), brand_header: @brand_headers)
    end
  end

  def last?(index)
    (index + 1) == model.length
  end
end
