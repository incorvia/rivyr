class ProductActionsCell < ApplicationCell
  def show(current_user: nil)
    @current_user = current_user
    render
  end

  private

  property :name
  property :amazon_url
  property :price
end
