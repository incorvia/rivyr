class FollowLinkCell < ApplicationCell

  def show(current_user: nil, product: nil, text: nil)
    @current_user = current_user
    @product = product
    @text = text
    render
  end

  private

  def following?
    if current_user && @product
      return current_user.following?(@product)
    end
  end
end
