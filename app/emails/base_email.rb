class BaseEmail
  attr_accessor :template, :template_content, :message

  def self.mandrill
    @mandrill ||= Mandrill::API.new
  end

  def initialize(user)
    @user = user
  end

  def send_email
    @user.update_attribute(:last_newsletter_at, Time.now)
  end

  def content
    # implement in subclass
  end

  def template
    'stream-update'
  end

  def template_content
    @template_content ||= [{"name" => "main_content", "content" => content}]
  end

  def message
    @message ||= { 
      to: [
        { email: @user.email }
      ],
      merge_vars: [{
        rcpt: @user.email,
        vars: [
          { 
            name: 'user_email',
            content: @user.email
          }
        ]
      }]
    }
  end

  def routes
    Rails.application.routes.url_helpers
  end

  protected

  def controller
    @controller ||= ApplicationController.new
  end

  def view
    @view ||= ActionView::Base.new('app/views/emails', {}, self.controller)
  end
end
