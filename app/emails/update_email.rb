class UpdateEmail < BaseEmail
  def send_email
    if droplets.any?
      self.class.mandrill.messages.send_template(template, template_content, message)
    end
    super
  end

  def droplets
    @droplets ||= @user.latest_updates(10)
  end

  def content
    view.render partial: "/newsletter/update.slim", locals: { droplets: droplets, routes: routes, host: configatron.default_host }
  end
end
