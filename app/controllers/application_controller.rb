class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :force_authentication
  before_filter :update_last_sign_in_at

  helper_method :js_env

  def force_authentication
    if params[:force_auth]
      authenticate_user! 
    end
  end

  def js_env
    @js_env ||= {
      environment: Rails.env,
      env: {
        algoliasearch_application_id: configatron.env.algoliasearch_application_id,
        algoliasearch_api_key_search: configatron.env.algoliasearch_api_key_search
      }
    }
  end

  protected

  def update_last_sign_in_at
    if user_signed_in? && !session[:logged_sign_in]
      sign_in(current_user, :force => true)
      session[:logged_sign_in] = true
    end
  end
end
