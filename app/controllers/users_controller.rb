class UsersController < ApplicationController
  include StreamControllable

  before_filter :authenticate_user!, only: [:show]

  def show
    @title = "Rivyr: User Stream"
    @stream = cell(:stream, droplets)
    @sidebar_cells = [
      cell(:facet_set, current_user).show(params_key: :type, filters: current_user.droplet_types_with_counts)
    ]
  end

  def model
    current_user
  end
end
