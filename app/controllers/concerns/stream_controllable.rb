module StreamControllable
  extend ActiveSupport::Concern 

  private

  def type
    params[:type]
  end

  def page
    @page ||= (params[:page] || 1)
  end

  def droplets
    @droplets ||= model.stream(type).page(page)
  end
end
