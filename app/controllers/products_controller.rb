class ProductsController < ApplicationController
  include StreamControllable

  before_filter :authenticate_user!, only: [:follow, :unfollow]

  def show
    @stream = cell(:stream, droplets)
    @title = "Rivyr: #{product.name} Stream"
    @sidebar_cells = [
      cell(:product_actions, @product).show(current_user: current_user),
      cell(:facet_set, @product).show(params_key: :type, filters: @product.droplet_types_with_counts, path: product_path(@product)),
      cell(:sidebar_ad).show(current_user: current_user)
    ]
  end

  def follow
    current_user.follow(product)
    render json: { following: true }
  end

  def unfollow
    current_user.unfollow(product)
    render json: { following: false }
  end

  private

  def model
    product
  end

  def product
    @product ||= Product.find(params[:id])
  end
end
