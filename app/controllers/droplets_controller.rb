class DropletsController < ApplicationController

  def show
    @title = "Rivyr: #{product.name} comments"
    @droplet_cell = cell(:droplet, droplet)
    @comment = Comment.new
    @comments = droplet.comment_hash
    @sidebar_cells = [
      cell(:product_actions, @product).show(current_user: current_user),
    ]
  end

  private

  def droplet
    @droplet ||= product.stream.find_by_slug!(params[:id])
  end

  def product
    @product ||= Product.find(params[:product_id])
  end
end
