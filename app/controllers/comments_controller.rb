class CommentsController < ApplicationController
  before_filter :authenticate_user!

  def create
    droplet = Droplet.find(params[:droplet_id])
    @comment = Comment.new(droplet: droplet, user: current_user, body: params[:comment][:body])
    if @comment.save
      redirect_to product_droplet_path(droplet.product, droplet)
    else
    end
  end
end
