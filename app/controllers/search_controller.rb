class SearchController < ApplicationController
  def index
    @q = params[:q] || ""
    @title = "Rivyr: Search for #{@q}"

    query = {}
    query[:facets] = ['category'.freeze]
    query[:tagFilters] = params[:category] if params[:category]
    
    response = Product.rivyr_raw(@q, query)

    @products = Product.where(id: response['hit_ids'.freeze]).includes(:vanity)
    @facets = response['facets'.freeze]

    @sidebar_cells = [
      cell(:facet_set, current_user).show(path: search_path, params_key: :category, filters: FacetSet.new(@facets['category'], ProductCategory), q: @q)
    ]
  end
end
