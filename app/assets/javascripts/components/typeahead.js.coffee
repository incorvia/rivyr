class @Typeahead
  $('#typeahead').typeahead null,
    name: "products"
    source: Search.engine.initIndex("Product_#{js_env.environment}").ttAdapter()
    templates:
      suggestion: (hit) ->
        """
        <p>#{hit._highlightResult.name.value}</p>
        """

  .on "typeahead:selected", (event, hit, dataset) ->
    window.location = hit.url
