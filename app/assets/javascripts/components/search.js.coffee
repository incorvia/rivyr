class @Search
  @engine = new AlgoliaSearch(js_env.env.algoliasearch_application_id, js_env.env.algoliasearch_api_key_search)

  @redirect_to_keyword = (keyword) ->
    if keyword
      window.location = "/search?q=#{keyword}"
    else
      window.location = "/search"

  $("#search-submit").click (e) ->
    keyword = $(@).closest('form').find('#typeahead')[0].value
    Search.redirect_to_keyword(keyword)
    e.preventDefault()

  $("#search-form").submit (e) ->
    keyword = $(@).find('#typeahead')[0].value
    Search.redirect_to_keyword(keyword)
    e.preventDefault()
