class @FollowLink
  @run = ->
    link = new FollowLink()
    link.run()

  constructor: ->
    @el = $('.cmp-follow-link')
    @icon = @el.find('.follow-icon')
    @status = @el.data('following')
    @product_id = @el.data('product-id')

    if @el.length > 0
      @success({ following: @status })

  run: =>
    @el.click =>
      $.ajax
        type: "POST"
        url: @url
        success: @success
        error: @error
        dataType: 'json'

  success: (data, code, request) =>
    @el.blur()

    if data['following']
      @status = true
      @icon.addClass('following')
      @icon.removeClass('not-following')
      @url = AppRoutes.unfollow_product_path(@product_id)
    else
      @status = false
      @icon.removeClass('following')
      @icon.addClass('not-following')
      @url = AppRoutes.follow_product_path(@product_id)

  error: (request, status, err) ->
    if err == "Unauthorized"
      window.location = AppRoutes.new_user_session_path()
