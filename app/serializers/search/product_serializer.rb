module Search
  class ProductSerializer < ActiveModel::Serializer
    attributes :name

    def self.mapping
      {
        product: {
          properties: {
            name: {
              type: "multi_field",
              fields: {
                feed: {
                  type: "string",
                  analyzer: "feed_analyzer" 
                },
                feed_keywords: {
                  type: 'string',
                  analyzer: "feed_keyword_analyzer" 
                }
              }
            }
          }
        }
      }
    end
  end
end

