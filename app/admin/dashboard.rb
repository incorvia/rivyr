ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Top Products" do
          ul do
            Product.all.order(cached_follower_total: :desc).limit(20).map do |product|
              li link_to("#{product.name}: #{product.cached_follower_total}", admin_product_path(product))
            end
          end
        end
      end

      column do
        panel "Info" do
          para "Welcome to ActiveAdmin."
        end
      end
    end
  end
end
