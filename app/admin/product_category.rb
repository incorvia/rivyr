ActiveAdmin.register ProductCategory do
  menu parent: 'Products'

  permit_params do
    [:name, :slug, :icon]
  end
end
