ActiveAdmin.register DropletType do
  menu parent: 'Content'

  index do
    column :id
    column :name
    column :slug
    column :icon
    column :created_at
    column :updated_at
    actions
  end

  permit_params do
    [:name, :slug, :icon]
  end
end
