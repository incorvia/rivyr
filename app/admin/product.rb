ActiveAdmin.register Product do
  menu parent: 'Products'

  filter :product_category
  filter :name
  filter :slug
  filter :asin
  filter :price_cents
  filter :created_at
  filter :updated_at

  scope :vanityless
  scope :asinless
  
  index do
    selectable_column
    column :id
    column :name
    column :slug
    column :asin
    column :price
    column :created_at
    column :updated_at
    column "Amazon" do |product|
      link_to "Amazon", "http://www.amazon.com/s/?field-keywords=#{product.name}", target: "_blank"
    end
    column "Site" do |product|
      link_to product.name, product_path(id: product.slug), target: "_blank"
    end
    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :product_category
      f.input :vanity_id
      f.input :name
      f.input :slug
      f.input :asin
      f.input :price
    end
    f.actions
  end

  permit_params do
    [:product_category_id, :vanity_id, :name, :slug, :asin, :price]
  end
end
