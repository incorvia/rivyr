ActiveAdmin.register User do
  menu parent: "People"

  filter :email
  filter :username
  filter :reset_password_sent_at
  filter :remember_created_at
  filter :last_sign_in_at
  filter :last_newsletter_at
  filter :created_at
  filter :updated_at

  index do
    column :id
    column :email
    column :username
    column :last_sign_in_at
    column :last_newsletter_at
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    inputs 'Details' do
      input :email
      input :username
      input :password, as: :password
      input :password_confirmation, as: :password
    end
    actions
  end

  permit_params do
    [:email, :password, :password_confirmation, :username]
  end
end
