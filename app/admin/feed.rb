ActiveAdmin.register Feed do
  menu parent: "Content"

  permit_params do
    [:name, :url]
  end
end
