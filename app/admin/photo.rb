ActiveAdmin.register Photo do

  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :description
      f.input :image_url
    end
    f.actions
  end
  
  permit_params do
    [:name, :description, :image_url]
  end
end
